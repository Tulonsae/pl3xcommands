package net.pl3x.pl3xcommands;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;

import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.BetterPluginInfo;
import Pl3xCommands.MyPlugin;

public class Economy {
	private MyPlugin plugin;
	private final HashMap<UUID, Double> balances = new HashMap<UUID, Double>();

	public Economy(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public Double getBalance(UUID uuid) {
		synchronized (balances) {
			if (!balances.containsKey(uuid)) {
				Double startBalance = plugin.getConfig().getDouble("starting-balance", 0D);
				Double balance = PlayerConfig.getConfig(plugin, uuid).getDouble("balance", startBalance);
				balances.put(uuid, balance);
				PlayerConfig.getConfig(plugin, uuid).set("balance", balance.toString());
			}
			return Math.round(balances.get(uuid) * 100.0) / 100.0;
		}
	}

	public void setBalance(UUID uuid, Double balance) {
		synchronized (balances) {
			balances.put(uuid, balance);
			PlayerConfig.getConfig(plugin, uuid).set("balance", Double.toString(Math.round(balance * 100.0) / 100.0));
		}
	}

	public boolean addAmount(UUID uuid, Double amount) {
		Double balance = getBalance(uuid);
		if (amount < 0) {
			return false;
		}
		balance += amount;
		setBalance(uuid, balance);
		return true;
	}

	public boolean subtractAmount(UUID uuid, Double amount) {
		Double balance = getBalance(uuid);
		if (balance < amount) {
			return false;
		}
		balance -= amount;
		setBalance(uuid, balance);
		return true;
	}

	public boolean trade(UUID sender, UUID target, Double amount) {
		if (amount < 0) {
			return false;
		}
		Double senderBalance = getBalance(sender);
		if (senderBalance < amount) {
			return false;
		}
		Double targetBalance = getBalance(target);
		setBalance(sender, senderBalance - amount);
		setBalance(target, targetBalance + amount);
		return true;
	}

	public TreeMap<Double, UUID> getTopBalances() {
		TreeMap<Double, UUID> allBalances = new TreeMap<Double, UUID>();
		for (File configFile : new File(new BetterPluginInfo(plugin.getPluginInfo()).getPluginDirectory(), "userdata").listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".ini");
			}
		})) {
			UUID uuid = UUID.fromString(configFile.getName().split(".ini")[0]);
			allBalances.put(plugin.getEconomy().getBalance(uuid), uuid);
		}
		TreeMap<Double, UUID> topBalances = new TreeMap<Double, UUID>(Collections.reverseOrder());
		int count = 1;
		for (Iterator<Entry<Double, UUID>> iter = allBalances.entrySet().iterator(); iter.hasNext();) {
			if (count >= 10) {
				break;
			}
			Map.Entry<Double, UUID> entry = iter.next();
			topBalances.put(Math.round(entry.getKey() * 100.0) / 100.0, entry.getValue());
			count++;
		}
		return topBalances;
	}
}
