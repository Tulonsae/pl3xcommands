package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.MailBox;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Player;

public class MailChecker extends Pl3xRunnable {
	private MyPlugin plugin;

	public MailChecker(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (plugin.getConfig().getBoolean("debug-mode", false)) {
			plugin.getLogger().debug("[TASK] Checking mail.");
		}
		for (MC_Player player : plugin.getServer().getPlayers()) {
			MailBox mailbox = MailBox.getMailBox(plugin, player.getUUID());
			if (!mailbox.refreshMail().isEmpty()) {
				Pl3xLibs.sendMessage(player, Lang.MAIL_NOTIFICATION_ALERT.get());
			}
		}
	}
}
