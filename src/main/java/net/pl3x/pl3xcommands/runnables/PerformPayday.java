package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Player;

public class PerformPayday extends Pl3xRunnable {
	private MyPlugin plugin;

	public PerformPayday(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		Double amount = plugin.getConfig().getDouble("payday-amount");
		Integer delay = plugin.getConfig().getInteger("payday-delay");
		if (amount == null || amount <= 0D || delay == null || delay <= 0) {
			if (plugin.getConfig().getBoolean("debug-mode", false)) {
				plugin.getLogger().debug("[TASK] Payday amount is 0, cancelling payday task!");
			}
			this.cancel();
			return;
		}
		if (plugin.getConfig().getBoolean("debug-mode", false)) {
			plugin.getLogger().debug("[TASK] Payday!");
		}
		String econSymbol = plugin.getConfig().get("econ-symbol");
		for (MC_Player player : plugin.getServer().getPlayers()) {
			plugin.getEconomy().addAmount(player.getUUID(), amount);
			Pl3xLibs.sendMessage(player, Lang.PAYDAY_PLAYER_MSG.get().replace("{econ-symbol}", econSymbol).replace("{amount}", amount.toString()));
		}
	}
}
