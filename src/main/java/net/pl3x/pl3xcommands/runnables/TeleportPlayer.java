package net.pl3x.pl3xcommands.runnables;

import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class TeleportPlayer extends Pl3xRunnable {
	private MyPlugin plugin;
	private MC_Player player;
	private MC_Location location;

	public TeleportPlayer(MyPlugin plugin, MC_Player player, MC_Location location) {
		this.plugin = plugin;
		this.player = player;
		this.location = location;
	}

	@Override
	public void run() {
		if (player == null || location == null) {
			if (plugin.getConfig().getBoolean("debug-mode", false)) {
				plugin.getLogger().debug("Tried to teleport a player, but failed...");
			}
		} else {
			player.teleport(location);
		}
		player = null;
		location = null;
		this.cancel(); // do not repeat
	}
}
