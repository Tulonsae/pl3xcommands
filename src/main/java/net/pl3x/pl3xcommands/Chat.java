package net.pl3x.pl3xcommands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import net.pl3x.pl3xcommands.commands.SocialSpy;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class Chat {
	private MyPlugin plugin;
	private Set<UUID> muted = new HashSet<UUID>();
	private Set<UUID> deaf = new HashSet<UUID>();
	private HashMap<UUID, List<UUID>> ignore = new HashMap<UUID, List<UUID>>();
	private HashMap<UUID, ChatChannel> channel = new HashMap<UUID, ChatChannel>();

	public Chat(MyPlugin plugin) {
		this.plugin = plugin;
	}

	public void sendChat(MC_Player sender, ChatChannel type, String message) {
		if (isPlayerMuted(sender.getUUID())) {
			Pl3xLibs.sendMessage(sender, Lang.MUTE_YOU_ARE_MUTED.get());
			return;
		}
		if (message == null || message.trim().equals("")) {
			Pl3xLibs.sendMessage(sender, Lang.ERROR_CHAT_BLANK_MSG.get());
			return;
		}
		message = message.trim();
		if (type.equals(ChatChannel.DEFAULT) && channel.containsKey(sender.getUUID())) {
			switch (channel.get(sender.getUUID())) {
				case GLOBAL:
					type = ChatChannel.GLOBAL;
					break;
				case UNIVERSE:
					type = ChatChannel.UNIVERSE;
					break;
				case LOCAL:
				default:
					type = ChatChannel.LOCAL;
			}
		}
		channel.put(sender.getUUID(), type);
		String prefix = plugin.getConfig().get("local-prefix", "&e[&7Local&e]");
		List<MC_Player> recipients = null;
		switch (type) {
			case GLOBAL:
				prefix = plugin.getConfig().get("global-prefix", "&e[&aGlobal&e]");
				recipients = getRecipients(sender.getWorld());
				break;
			case UNIVERSE:
				prefix = plugin.getConfig().get("universe-prefix", "&e[&bUniverse&e]");
				recipients = plugin.getServer().getPlayers();
				break;
			case LOCAL:
			default:
				Integer radius = plugin.getConfig().getInteger("local-chat-radius", 100);
				if (radius == null || radius == 0) {
					recipients = plugin.getServer().getPlayers();
				} else {
					recipients = Pl3xLibs.getNearbyPlayers(sender, radius);
					recipients.add(sender);
				}
		}
		for (MC_Player target : recipients) {
			if (isPlayerDeaf(target.getUUID()) && !target.getCustomName().equals(sender.getCustomName())) {
				continue; // target is deaf
			}
			if (isIgnoring(target.getUUID(), sender.getUUID())) {
				continue; // target is ignoring sender
			}
			Pl3xLibs.sendMessage(target, prefix + " &r" + sender.getCustomName() + "&7: &r" + message);
		}
		String spyPrefix = plugin.getConfig().get("spy-prefix", "&c[&6Spy&c]");
		for (MC_Player spy : getSpyRecipients(recipients)) {
			if (spy.getCustomName().equals(sender.getCustomName())) {
				continue; // Do not spy on self
			}
			if (isPlayerDeaf(spy.getUUID())) {
				continue; // spy is deaf
			}
			if (isIgnoring(spy.getUUID(), sender.getUUID())) {
				continue; // target is ignoring sender
			}
			Pl3xLibs.sendMessage(spy, spyPrefix + prefix + " &r" + sender.getCustomName() + "&7: &r" + message);
		}
	}

	private List<MC_Player> getRecipients(MC_World world) {
		String worldName = world.getName();
		List<MC_Player> list = new ArrayList<MC_Player>();
		for (MC_Player target : plugin.getServer().getPlayers()) {
			if (!target.getWorld().getName().equals(worldName)) {
				continue; // not in same world, skip
			}
			list.add(target);
		}
		return list;
	}

	private List<MC_Player> getSpyRecipients(List<MC_Player> recipients) {
		List<MC_Player> list = new ArrayList<MC_Player>();
		for (MC_Player target : plugin.getServer().getPlayers()) {
			if (recipients.contains(target)) {
				continue; // already a recipient; no need to spy
			}
			if (!SocialSpy.socialSpyMode.contains(target.getUUID())) {
				continue; // not spying
			}
			list.add(target);
		}
		return list;
	}

	public void mutePlayer(UUID uuid) {
		if (isPlayerMuted(uuid)) {
			return; // already muted
		}
		muted.add(uuid);
	}

	public void unmutePlayer(UUID uuid) {
		if (!isPlayerMuted(uuid)) {
			return; // not muted
		}
		muted.remove(uuid);
	}

	public boolean isPlayerMuted(UUID uuid) {
		return muted.contains(uuid);
	}

	public void deafenPlayer(UUID uuid) {
		if (isPlayerDeaf(uuid)) {
			return; // already deaf
		}
		deaf.add(uuid);
	}

	public void undeafenPlayer(UUID uuid) {
		if (!isPlayerDeaf(uuid)) {
			return; // not deaf
		}
		deaf.remove(uuid);
	}

	public boolean isPlayerDeaf(UUID uuid) {
		return deaf.contains(uuid);
	}

	public List<UUID> getIgnoreList(UUID uuid) {
		if (!ignore.containsKey(uuid))
			return new ArrayList<UUID>();
		return ignore.get(uuid);
	}

	public void setIgnoreList(UUID uuid, List<UUID> list) {
		this.ignore.put(uuid, list);
	}

	public boolean isIgnoring(UUID ignorer, UUID ignored) {
		return getIgnoreList(ignorer).contains(ignored);
	}

	public enum ChatChannel {
		DEFAULT,
		LOCAL,
		GLOBAL,
		UNIVERSE;
	}
}
