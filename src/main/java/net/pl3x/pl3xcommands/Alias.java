package net.pl3x.pl3xcommands;

import java.util.Arrays;
import java.util.List;

import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xCommands.MyPlugin;

public enum Alias {
	BACK("back", ""),
	BALANCE("balance", "bal"),
	BALANCETOP("balancetop", "baltop,btop"),
	BROADCAST("broadcast", "bc,broadc,bcast"),
	BUY("buy", "purchase"),
	CLEARINVENTORY("clearinventory", "ci,clearinv"),
	COORDS("coords", "pos,position,loc,location,coordinates"),
	DEAFEN("deafen", "deaf"),
	DELHOME("delhome", "dhome,rmhome,deletehome,removehome"),
	DELWARP("delwarp", "deletewarp"),
	ECON("econ", "economy,eco"),
	FLY("fly", ""),
	FLYSPEED("flyspeed", "fs"),
	GLOBAL("global", "g,gchat,globalchat"),
	GOD("god", "tgm,godmode"),
	HAT("hat", "helmet"),
	HOME("home", ""),
	IGNORE("ignore", "forget"),
	LISTHOMES("listhomes", "lshome,listhome,homes,lshomes,listhomes"),
	LOCAL("local", "l,lchat,localchat"),
	KILL("kill", "murder"),
	KILLALL("killall", "kall"),
	MAIL("mail", "email"),
	MOTD("motd", "messageoftheday"),
	MSG("msg", "message,send,tell,whisper,w,pm"),
	MUTE("mute", "silence"),
	MUTEALL("muteall", "silenceall"),
	NEAR("near", ""),
	NICK("nick", "nickname,displayname,name,changename"),
	PAY("pay", ""),
	PAYDAY("payday", "pd"),
	PING("ping", "pong"),
	PL3XCOMMANDS("pl3xcommands", ""),
	POWERTOOL("powertool", "pt,assign"),
	RENAME("rename", ""),
	REPLY("reply", "r"),
	RULES("rules", "serverrules"),
	SELL("sell", ""),
	SETHOME("sethome", "shome"),
	SETSPAWN("setspawn", ""),
	SETWARP("setwarp", ""),
	SETWORLDSPAWN("setworldspawn", ""),
	SETWORTH("setworth", ""),
	SMITE("smite", "strike,lightning"),
	SOCIALSPY("socialspy", "chatspy"),
	SPAWN("spawn", ""),
	SUDO("sudo", "force,simonsays"),
	SUICIDE("suicide", ""),
	TOP("top", ""),
	TELEPORT("teleport", "tp,port"),
	TP2P("tp2p", "teleportplayertoplayer,teleportplayer2player,tpp2p,teleportp2p,teleportptop"),
	TPA("tpa", "tpr,teleportrequest,tprequest,teleportr"),
	TPAALL("tpaall", "tprall,teleportaskall,teleportrequestall"),
	TPACCEPT("tpaccept", "tpallow,teleportallow,teleportaccept,teleportyes,tpyes,tpac"),
	TPAHERE("tpahere", "tprh,tpah,teleportrequesthere,teleportrhere,tphrequest,tphererequest,tprhere"),
	TPALL("tpall", "teleportall,teleporteveryone"),
	TPDENY("tpdeny", "teleportdeny,teleportno,tpno,tpde"),
	TPHERE("tphere", "teleporthere,tph"),
	TPPOS("tppos", "tpos,teleportposition,tpcoords,tpp,tpc,teleportcoordinates,teleportpos,tpposition,tposition"),
	TPTOGGLE("tptoggle", "teleporttoggle,toggletp,toggleteleport"),
	UNIVERSE("universe", "u,uchat,universechat"),
	WALKSPEED("walkspeed", "ws"),
	WARP("warp", ""),
	WORLD("world", "dimension"),
	WORTH("worth", "price,cost");

	private String key;
	private String def;

	private static BaseConfig config;

	private Alias(String key, String def) {
		this.key = key;
		this.def = def;
		init();
	}

	private static void init() {
		config = new BaseConfig(MyPlugin.class, MyPlugin.getInstance().getPluginInfo(), "", "aliases.ini");
		config.load();
	}

	public static void refreshAll() {
		config = null;
		init();
	}

	public List<String> get() {
		String value = config.get(key, def).replace(" ", "");
		if (value == null || value.equals("")) {
			// This shouldn't happen
			return Arrays.asList(new String[] {});
		}
		return Arrays.asList(value.split(","));
	}
}
