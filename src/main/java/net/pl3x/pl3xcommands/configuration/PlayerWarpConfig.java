package net.pl3x.pl3xcommands.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Location;

public class PlayerWarpConfig extends BaseConfig {
	public PlayerWarpConfig(MyPlugin plugin) {
		super(MyPlugin.class, plugin.getPluginInfo(), "", "warps.ini");
		load(false);
	}

	public String listWarps() {
		HashMap<String, String> map = getWarpsMap();
		if (map == null || map.isEmpty()) {
			return Lang.WARP_NO_WARPS_FOUND.get();
		}
		return Lang.WARP_LIST.get().replace("{list}", Pl3xLibs.join(map.keySet(), Lang.WARP_LIST_SEPARATOR.get(), 0));
	}

	public MC_Location getWarp(String name) {
		String locStr = getWarpsMap().get(name);
		if (locStr == null || locStr.equals("")) {
			return null;
		}
		return Pl3xLibs.stringToLocation(locStr);
	}

	public void addWarp(String name, MC_Location location) {
		HashMap<String, String> warps = getWarpsMap();
		warps.put(name, Pl3xLibs.locationToString(location));
		setWarpsMap(warps);
	}

	public void deleteWarp(String name) {
		HashMap<String, String> warps = getWarpsMap();
		warps.remove(name);
		setWarpsMap(warps);
	}

	public List<String> getMatchingWarpsList(String name) {
		List<String> matches = new ArrayList<String>();
		if (name == null) {
			return null;
		}
		name = name.toLowerCase().trim();
		for (String warp : getWarpsMap().keySet()) {
			if (warp.toLowerCase().contains(name)) {
				matches.add(warp);
			}
		}
		if (matches.isEmpty()) {
			return null;
		}
		return matches;
	}

	private HashMap<String, String> getWarpsMap() {
		return getMap("warps");
	}

	private void setWarpsMap(HashMap<String, String> map) {
		setMap("warps", map);
	}
}
