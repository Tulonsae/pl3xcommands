package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Near implements MC_Command {
	public Near(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.NEAR.get();
	}

	@Override
	public String getCommandName() {
		return "near";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7near &a- &d" + Lang.NEAR_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		List<MC_Player> nearby = Pl3xLibs.getNearbyPlayers(player, 100);
		if (nearby == null || nearby.isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.NEAR_NO_PLAYERS.get());
			return;
		}
		String[] nearbyNames = new String[nearby.size()];
		int count = 0;
		for (MC_Player target : nearby) {
			nearbyNames[count] = target.getCustomName();
			count++;
		}
		Pl3xLibs.sendMessage(player, Lang.NEAR_PLAYERS_LIST.get().replace("{list}", Pl3xLibs.join(nearbyNames, Lang.NEAR_PLAYERS_SEPARATOR.get(), 0)));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.ping");
	}
}
