package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Pay implements MC_Command {
	private MyPlugin plugin;

	public Pay(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.PAY.get();
	}

	@Override
	public String getCommandName() {
		return "pay";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7pay &e<&7player&e> &e<&7amount&e> &a- &d" + Lang.PAY_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 2) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		Double amount = 0D;
		try {
			amount = Double.valueOf(args[1]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.PAY_INVALID_NUMBER.get());
			return;
		}
		if (amount <= 0D) {
			Pl3xLibs.sendMessage(player, Lang.PAY_NEGATIVE_AMOUNT.get());
			return;
		}
		if (plugin.getEconomy().getBalance(player.getUUID()) < amount) {
			Pl3xLibs.sendMessage(player, Lang.PAY_NOT_ENOUGH_FUNDS.get());
			return;
		}
		if (!plugin.getEconomy().trade(player.getUUID(), target.getUUID(), amount)) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN.get());
			return;
		}
		String econSymbol = plugin.getConfig().get("econ-symbol");
		MC_Player online = Pl3xLibs.getPlayer(target.getCustomName());
		if (online != null) {
			Pl3xLibs.sendMessage(online, Lang.PAY_TARGET_MSG.get().replace("{player}", player.getCustomName()).replace("{econ-symbol}", econSymbol).replace("{amount}", amount.toString()));
		}
		Pl3xLibs.sendMessage(player, Lang.PAY_PLAYER_MSG.get().replace("{target}", target.getCustomName()).replace("{econ-symbol}", econSymbol).replace("{amount}", amount.toString()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.pay");
	}
}
