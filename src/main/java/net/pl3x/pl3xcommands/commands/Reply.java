package net.pl3x.pl3xcommands.commands;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Reply implements MC_Command {
	private MyPlugin plugin;
	public static HashMap<UUID, UUID> replydb = new HashMap<UUID, UUID>();

	public Reply(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.REPLY.get();
	}

	@Override
	public String getCommandName() {
		return "reply";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7reply &e<&7message&e> &a- &d" + Lang.REPLY_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 1) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String message = Pl3xLibs.join(args, " ", 0);
		if (message == null || message.equals("")) {
			Pl3xLibs.sendMessage(player, Lang.MSG_BLANK_MSG.get());
			return;
		}
		MC_Player target = null;
		synchronized (replydb) {
			if (!replydb.containsKey(player.getUUID())) {
				Pl3xLibs.sendMessage(player, Lang.REPLY_NO_TARGET.get());
				return;
			}
			target = Pl3xLibs.getPlayer(replydb.get(player.getUUID()));
		}
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (plugin.getChat().isIgnoring(target.getUUID(), player.getUUID())) {
			Pl3xLibs.sendMessage(player, Lang.IGNORE_YOU_ARE_IGNORED.get().replace("{target}", target.getCustomName()));
			return;
		}
		synchronized (replydb) {
			replydb.put(target.getUUID(), player.getUUID());
		}
		Pl3xLibs.sendMessage(target, Lang.REPLY_TARGET.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		Pl3xLibs.sendMessage(player, Lang.REPLY_PLAYER.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		for (MC_Player spy : plugin.getServer().getPlayers()) {
			if (spy.getCustomName().equals(target.getCustomName())) {
				continue; // target doesnt need to spy
			}
			if (spy.getCustomName().equals(player.getCustomName())) {
				continue; // sender doesnt need to spy
			}
			if (!SocialSpy.socialSpyMode.contains(spy.getUUID())) {
				continue; // not in spy mode
			}
			Pl3xLibs.sendMessage(spy, Lang.REPLY_OTHER.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName()).replace("{message}", message));
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.reply");
	}
}
