package net.pl3x.pl3xcommands.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.MailBox;
import net.pl3x.pl3xcommands.MailBox.Letter;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Mail implements MC_Command {
	private MyPlugin plugin;

	public Mail(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.MAIL.get();
	}

	@Override
	public String getCommandName() {
		return "mail";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player.hasPermission("command.mail.send")) {
			return Pl3xLibs.colorize("&e/&7mail &e<&7read|clear|send &e<&7player&e> <&7message&e>> &a- &d" + Lang.MAIL_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7mail &e<&7read|clear> &a- &d" + Lang.MAIL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			if ("clear".startsWith(args[0])) {
				return Arrays.asList(new String[] {
					"clear"
				});
			}
			if ("read".startsWith(args[0])) {
				return Arrays.asList(new String[] {
					"read"
				});
			}
			if ("send".startsWith(args[0])) {
				return Arrays.asList(new String[] {
					"send"
				});
			}
		}
		if (args.length == 2 && args[0].equalsIgnoreCase("send")) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 1) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String subCommand = args[0];
		if (subCommand.equalsIgnoreCase("read")) {
			Set<Letter> mail = MailBox.getMailBox(plugin, player.getUUID()).getMail();
			if (mail == null || mail.isEmpty()) {
				Pl3xLibs.sendMessage(player, Lang.MAIL_NO_MAIL.get());
				return;
			}
			for (Letter letter : mail) {
				Pl3xLibs.sendMessage(player, letter.format());
			}
		} else if (subCommand.equalsIgnoreCase("clear")) {
			MailBox.getMailBox(plugin, player.getUUID()).clearMail();
			Pl3xLibs.sendMessage(player, Lang.MAIL_CLEARED.get());
		} else if (subCommand.equalsIgnoreCase("send")) {
			if (!player.hasPermission("command.mail.send")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			if (args.length < 3) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
				Pl3xLibs.sendMessage(player, getHelpLine(player));
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(args[1]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.MAIL_NO_PLAYER.get());
				return;
			}
			if (plugin.getChat().isIgnoring(target.getUUID(), player.getUUID())) {
				Pl3xLibs.sendMessage(player, Lang.IGNORE_YOU_ARE_IGNORED.get().replace("{target}", target.getCustomName()));
				return;
			}
			MailBox.getMailBox(plugin, target.getUUID()).sendMail(player.getCustomName(), Pl3xLibs.join(args, " ", 2));
			Pl3xLibs.sendMessage(player, Lang.MAIL_SENT.get());
		} else {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
		}
		return;
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.mail");
	}
}
