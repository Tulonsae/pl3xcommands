package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class KillAll implements MC_Command {
	private MyPlugin plugin;

	public KillAll(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.KILLALL.get();
	}

	@Override
	public String getCommandName() {
		return "killall";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/killall - " + Lang.KILLALL_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7killall &a- &d" + Lang.KILLALL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		for (MC_Player target : plugin.getServer().getPlayers()) {
			target.kill();
		}
		plugin.getServer().broadcastMessage(Pl3xLibs.colorize(Lang.KILLALL_ANNOUNCE.get().replace("{player}", player.getCustomName())));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.killall");
	}
}
