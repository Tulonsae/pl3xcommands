package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class Coords implements MC_Command {
	public Coords(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.COORDS.get();
	}

	@Override
	public String getCommandName() {
		return "coords";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/coords (player) - " + Lang.COORDS_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.coords.other")) {
			return Pl3xLibs.colorize("&e/&7coords &e(&7player&e) &a- &d" + Lang.COORDS_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7coords &a- &d" + Lang.COORDS_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0) {
			if (player != null && !player.hasPermission("command.coords.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.coords.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.COORDS_TARGET_EXEMPT.get());
				return;
			}
			MC_Location location = target.getLocation();
			Pl3xLibs.sendMessage(player, Lang.COORDS_LOCATION_OTHER.get().replace("{target}", target.getCustomName()).replace("{world}", target.getWorld().getName()).replace("{x}", Integer.toString(location.getBlockX())).replace("{y}", Integer.toString(location.getBlockY())).replace("{z}", Integer.toString(location.getBlockZ())));
			return;
		}
		MC_Location location = player.getLocation();
		Pl3xLibs.sendMessage(player, Lang.COORDS_LOCATION.get().replace("{world}", player.getWorld().getName()).replace("{x}", Integer.toString(location.getBlockX())).replace("{y}", Integer.toString(location.getBlockY())).replace("{z}", Integer.toString(location.getBlockZ())));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.coords");
	}
}
