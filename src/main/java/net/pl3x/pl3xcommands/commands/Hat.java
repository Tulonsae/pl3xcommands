package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class Hat implements MC_Command {
	public Hat(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.HAT.get();
	}

	@Override
	public String getCommandName() {
		return "hat";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7hat &a- &d" + Lang.HAT_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		MC_ItemStack hat = player.getItemInHand();
		if (hat == null || hat.getId() == 0) {
			Pl3xLibs.sendMessage(player, Lang.HAT_EMPTY_HAND.get());
			return;
		}
		player.setItemInHand(null);
		List<MC_ItemStack> armor = player.getArmor();
		MC_ItemStack oldHat = armor.get(3);
		if (oldHat != null && oldHat.getId() != 0) {
			player.setItemInHand(oldHat);
		}
		armor.set(3, hat);
		player.setArmor(armor);
		player.updateInventory();
		Pl3xLibs.sendMessage(player, Lang.HAT_PLAYER_MSG.get().replace("{material}", hat.getFriendlyName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.hat");
	}
}
