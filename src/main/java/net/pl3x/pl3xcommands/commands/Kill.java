package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Kill implements MC_Command {
	private MyPlugin plugin;

	public Kill(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.KILL.get();
	}

	@Override
	public String getCommandName() {
		return "kill";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/kill <player> - " + Lang.KILL_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7kill &e<&7player&e> &a- &d" + Lang.KILL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 1) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		target.kill();
		plugin.getServer().broadcastMessage(Pl3xLibs.colorize(Lang.KILL_ANNOUNCE.get().replace("{player}", player.getCustomName()).replace("{target}", target.getCustomName())));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.kill");
	}
}
