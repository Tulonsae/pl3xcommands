package net.pl3x.pl3xcommands.commands;

import java.util.HashMap;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class Back implements MC_Command {
	public static HashMap<String, MC_Location> backdb = new HashMap<String, MC_Location>();

	public Back(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.BACK.get();
	}

	@Override
	public String getCommandName() {
		return "back";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7back &a- &d" + Lang.BACK_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (!backdb.containsKey(player.getCustomName())) {
			Pl3xLibs.sendMessage(player, Lang.BACK_NO_LOCATION.get());
			return;
		}
		MC_Location previous = backdb.get(player.getCustomName());
		if (previous == null) {
			// This should not happen
			Pl3xLibs.sendMessage(player, Lang.BACK_NO_LOCATION.get());
			backdb.remove(player.getCustomName());
			return;
		}
		player.teleport(previous);
		Pl3xLibs.sendMessage(player, Lang.BACK_TELEPORTED.get());
		backdb.remove(player.getCustomName());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.back");
	}
}
