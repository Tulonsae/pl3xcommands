package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class SetSpawn implements MC_Command {
	public SetSpawn(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.SETSPAWN.get();
	}

	@Override
	public String getCommandName() {
		return "setspawn";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7setspawn &a- &d" + Lang.SETSPAWN_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		Pl3xLibs.setServerSpawn(player.getLocation());
		Pl3xLibs.sendMessage(player, Lang.SETSPAWN_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.setspawn");
	}
}
