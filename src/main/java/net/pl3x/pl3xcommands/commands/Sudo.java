package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Sudo implements MC_Command {
	public Sudo(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.SUDO.get();
	}

	@Override
	public String getCommandName() {
		return "sudo";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/sudo <player> <command> - " + Lang.SUDO_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7sudo &e<&7player&e> <&7command&e> &a- &d" + Lang.SUDO_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 2) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getPlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		String command = Pl3xLibs.join(args, " ", 1);
		if (command == null || command.equals("")) {
			Pl3xLibs.sendMessage(player, Lang.SUDO_BLANK_COMMAND.get());
			return;
		}
		if (command.startsWith("/")) {
			Pl3xLibs.sendMessage(player, Lang.SUDO_COMMAND_START_SLASH.get());
			return;
		}
		Pl3xLibs.sendMessage(player, Lang.SUDO_PLAYER_MSG.get().replace("{target}", target.getCustomName()).replace("{command}", "/" + command));
		target.executeCommand("/" + command);
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.sudo");
	}
}
