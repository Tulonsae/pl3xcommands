package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Balance implements MC_Command {
	private MyPlugin plugin;

	public Balance(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.BALANCE.get();
	}

	@Override
	public String getCommandName() {
		return "balance";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/balance (player) - " + Lang.BALANCE_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.balance.other")) {
			return Pl3xLibs.colorize("&e/&7balance &e(&7player&e) &a- &d" + Lang.BALANCE_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7balance &a- &d" + Lang.BALANCE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		String econSymbol = plugin.getConfig().get("econ-symbol");
		if (args.length > 0) {
			if (player != null && !player.hasPermission("command.balance.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			// No exemptions!
			Double balance = plugin.getEconomy().getBalance(target.getUUID());
			Pl3xLibs.sendMessage(player, Lang.BALANCE_CHECK_OTHER.get().replace("{econ-symbol}", econSymbol).replace("{amount}", balance.toString()).replace("{target}", target.getCustomName()));
			return;
		}
		Double balance = plugin.getEconomy().getBalance(player.getUUID());
		Pl3xLibs.sendMessage(player, Lang.BALANCE_CHECK.get().replace("{econ-symbol}", econSymbol).replace("{amount}", balance.toString()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.balance");
	}
}
