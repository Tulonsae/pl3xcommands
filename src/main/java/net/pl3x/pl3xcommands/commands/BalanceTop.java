package net.pl3x.pl3xcommands.commands;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class BalanceTop implements MC_Command {
	private MyPlugin plugin;

	public BalanceTop(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.BALANCETOP.get();
	}

	@Override
	public String getCommandName() {
		return "balancetop";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/balancetop - " + Lang.BALANCETOP_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7balancetop &a- &d" + Lang.BALANCETOP_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		TreeMap<Double, UUID> top = plugin.getEconomy().getTopBalances();
		for (Iterator<Entry<Double, UUID>> iter = top.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<Double, UUID> entry = iter.next();
			Pl3xLibs.sendMessage(player, Lang.BALANCETOP_ENTRY.get().replace("{player}", Pl3xLibs.getOfflinePlayer(entry.getValue()).getCustomName()).replace("{amount}", entry.getKey().toString()));
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.balancetop");
	}
}
