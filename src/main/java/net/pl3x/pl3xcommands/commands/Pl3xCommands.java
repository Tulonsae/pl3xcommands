package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Pl3xCommands implements MC_Command {
	private MyPlugin plugin;

	public Pl3xCommands(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.PL3XCOMMANDS.get();
	}

	@Override
	public String getCommandName() {
		return "pl3xcommands";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/pl3xcommands (reload) - " + Lang.PL3XCOMMANDS_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7pl3xcommands &e(&7reload&e) &a- &d" + Lang.PL3XCOMMANDS_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			plugin.reload();
			Pl3xLibs.sendMessage(player, Lang.RELOAD.get());
			return;
		}
		Pl3xLibs.sendMessage(player, "&d" + plugin.getPluginInfo().name + " v&7" + plugin.getPluginInfo().version + "&d.");
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.pl3xcommands");
	}
}
