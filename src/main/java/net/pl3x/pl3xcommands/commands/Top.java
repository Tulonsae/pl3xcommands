package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_World;

public class Top implements MC_Command {
	private MyPlugin plugin;

	public Top(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TOP.get();
	}

	@Override
	public String getCommandName() {
		return "top";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7top &a- &d" + Lang.TOP_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		MC_Location location = player.getLocation();
		MC_World world = player.getWorld();
		double x = location.x;
		double z = location.z;
		int y = location.getBlockY();
		int d = location.dimension;
		float pitch = location.pitch;
		float yaw = location.yaw;
		for (int j = plugin.getServer().getMaxBuildHeight(); j >= y; j--) {
			if (world.getBlockAt((int) x, j, (int) z).isSolid()) {
				player.teleport(new MC_Location(x, j + 1, z, d, yaw, pitch));
				return;
			}
		}
		Pl3xLibs.sendMessage(player, Lang.TOP_ALREADY_HIGHEST.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.top");
	}
}
