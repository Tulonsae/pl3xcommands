package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Tpall implements MC_Command {
	private MyPlugin plugin;

	public Tpall(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPALL.get();
	}

	@Override
	public String getCommandName() {
		return "tpall";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7tpall &a- &d" + Lang.TPALL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		for (MC_Player target : plugin.getServer().getPlayers()) {
			if (target.equals(player)) {
				continue; // do not teleport self
			}
			if (PlayerConfig.getConfig(plugin, target.getUUID()).getBoolean("tp-disabled") && !player.hasPermission("command.tp.override")) {
				continue; // target has tp disabled
			}
			target.teleport(player.getLocation());
			Pl3xLibs.sendMessage(target, Lang.TPHERE_TARGET_MSG.get());
		}
		Pl3xLibs.sendMessage(player, Lang.TPALL_PLAYER_MSG.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tpall");
	}
}
