package net.pl3x.pl3xcommands.commands;

import java.util.HashMap;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class ListHomes implements MC_Command {
	private MyPlugin plugin;

	public ListHomes(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.LISTHOMES.get();
	}

	@Override
	public String getCommandName() {
		return "listhomes";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/listhomes (player) - " + Lang.LISTHOMES_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.listhomes.other")) {
			return Pl3xLibs.colorize("&e/&listhomes &e(&7player&e) &a- &d" + Lang.LISTHOMES_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&listhomes &a- &d" + Lang.LISTHOMES_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		HashMap<String, String> homes = new HashMap<String, String>();
		if (args.length > 0) {
			if (!player.hasPermission("command.listhomes.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.listhomes.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.LISTHOMES_TARGET_EXEMPT.get());
				return;
			}
			homes = PlayerConfig.getConfig(plugin, target.getUUID()).getMap("homes");
		} else {
			homes = PlayerConfig.getConfig(plugin, player.getUUID()).getMap("homes");
		}
		if (homes.isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.LISTHOMES_NO_HOMES.get());
			return;
		}
		String homelist = Pl3xLibs.join(homes.keySet(), Lang.LISTHOMES_SEPARATOR.get(), 0);
		Pl3xLibs.sendMessage(player, Lang.LISTHOMES_LIST.get().replace("{list}", homelist));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.listhomes");
	}
}
