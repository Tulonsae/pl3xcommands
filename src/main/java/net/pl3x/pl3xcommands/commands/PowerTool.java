package net.pl3x.pl3xcommands.commands;

import java.util.HashMap;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Player;

public class PowerTool implements MC_Command {
	private MyPlugin plugin;

	public PowerTool(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.POWERTOOL.get();
	}

	@Override
	public String getCommandName() {
		return "powertool";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7powertool &e<&7command&e> &a- &d" + Lang.POWERTOOL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		// TODO Generate available list of commands
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		Pl3xLibs.sendMessage(player, "&4This command is not yet implemented!");
		// TODO
		boolean disabled = true;
		if (disabled) {
			// disabled temporarily
			return;
		}
		MC_ItemStack hand = player.getItemInHand();
		if (hand == null || hand.getId() == 0) {
			Pl3xLibs.sendMessage(player, Lang.POWERTOOL_NO_ITEM.get());
			return;
		}
		PlayerConfig config = PlayerConfig.getConfig(plugin, player.getUUID());
		HashMap<String, String> tools = config.getPowerTools();
		String currentTool = tools.get(hand.getFriendlyName());
		if (args.length == 0) {
			if (currentTool == null || currentTool.equals("")) {
				Pl3xLibs.sendMessage(player, Lang.POWERTOOL_NO_CURRENT_TOOL.get());
			} else {
				Pl3xLibs.sendMessage(player, Lang.POWERTOOL_CURRENT_TOOL.get().replace("{command}", currentTool));
			}
			return;
		}
		tools.put(hand.getFriendlyName(), Pl3xLibs.join(args, " ", 0));
		config.setPowerTools(tools);
		Pl3xLibs.sendMessage(player, Lang.POWERTOOL_TOOL_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.powertool");
	}
}
