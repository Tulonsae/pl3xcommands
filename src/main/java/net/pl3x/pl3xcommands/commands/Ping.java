package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Ping implements MC_Command {
	public Ping(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.PING.get();
	}

	@Override
	public String getCommandName() {
		return "ping";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/ping - " + Lang.PING_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7ping &a- &d" + Lang.PING_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		Pl3xLibs.sendMessage(player, Lang.PING_PONG.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.ping");
	}
}
