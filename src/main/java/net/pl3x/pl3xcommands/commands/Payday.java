package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.runnables.PerformPayday;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.scheduler.Task;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Payday implements MC_Command {
	private MyPlugin plugin;
	public static Task task;

	public Payday(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.PAYDAY.get();
	}

	@Override
	public String getCommandName() {
		return "payday";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7payday &a- &d" + Lang.PAYDAY_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (task != null) {
			task.cancel();
		}
		Pl3xLibs.sendMessage(player, Lang.PAYDAY_TRIGGERED.get());
		task = Pl3xLibs.getScheduler().scheduleRepeatingTask(plugin.getPluginInfo().name, new PerformPayday(plugin), 0, plugin.getConfig().getInteger("payday-delay", 3600) * 20);
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.payday");
	}
}
