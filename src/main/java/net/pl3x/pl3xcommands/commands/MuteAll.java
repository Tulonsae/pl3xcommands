package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class MuteAll implements MC_Command {
	private MyPlugin plugin;
	private boolean globalMute = false;

	public MuteAll(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.MUTEALL.get();
	}

	@Override
	public String getCommandName() {
		return "muteall";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/muteall - " + Lang.MUTEALL_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7muteall &a- &d" + Lang.MUTEALL_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		globalMute = !globalMute;
		for (MC_Player target : plugin.getServer().getPlayers()) {
			if (target.hasPermission("command.mute.exempt")) {
				continue; // target is exempt
			}
			if (target.getCustomName().equals(player.getCustomName())) {
				continue; // do not mute self
			}
			if (globalMute) {
				plugin.getChat().mutePlayer(target.getUUID());
			} else {
				plugin.getChat().unmutePlayer(target.getUUID());
			}
			Pl3xLibs.sendMessage(target, Lang.MUTE_TARGET_MSG.get().replace("{action}", globalMute ? "temporarily muted" : "unmuted"));
		}
		Pl3xLibs.sendMessage(player, Lang.MUTEALL_PLAYER_MSG.get().replace("{action}", globalMute ? "temporarily muted" : "unmuted"));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.muteall");
	}
}
