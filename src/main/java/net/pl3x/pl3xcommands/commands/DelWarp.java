package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class DelWarp implements MC_Command {
	private MyPlugin plugin;

	public DelWarp(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.DELWARP.get();
	}

	@Override
	public String getCommandName() {
		return "delwarp";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/delwarp <warp> - " + Lang.DELWARP_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7delwarp &e<&7warp&e> &a- &d" + Lang.DELWARP_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return plugin.getWarpConfig().getMatchingWarpsList(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.DELWARP_NO_NAME.get());
			return;
		}
		String warpName = args[0].trim().toLowerCase();
		if (plugin.getWarpConfig().getWarp(warpName) == null) {
			Pl3xLibs.sendMessage(player, Lang.DELWARP_NO_WARP.get());
			return;
		}
		plugin.getWarpConfig().deleteWarp(warpName);
		Pl3xLibs.sendMessage(player, Lang.DELWARP_DELETED.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.delwarp");
	}
}
