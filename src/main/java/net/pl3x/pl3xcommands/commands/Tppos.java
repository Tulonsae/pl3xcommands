package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Location;
import PluginReference.MC_Player;

public class Tppos implements MC_Command {
	public Tppos(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.TPPOS.get();
	}

	@Override
	public String getCommandName() {
		return "tppos";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		return Pl3xLibs.colorize("&e/&7tppos &e<&7x&e> <&7y&e> <&7z&e> &a- &d" + Lang.TPPOS_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 3) {
			Pl3xLibs.sendMessage(player, Lang.TPPOS_NO_COORDS.get());
			return;
		}
		double x, y, z;
		try {
			x = Double.valueOf(args[0]);
			y = Double.valueOf(args[1]);
			z = Double.valueOf(args[2]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.TPPOS_INVALID_COORDS.get());
			return;
		}
		MC_Location location = player.getLocation();
		location.x = x; // this seems very crude...
		location.y = y;
		location.z = z;
		player.teleport(location);
		Pl3xLibs.sendMessage(player, Lang.TPPOS_TELEPORTED.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return false; // NO CONSOLE
		}
		return player.hasPermission("command.tppos");
	}
}
