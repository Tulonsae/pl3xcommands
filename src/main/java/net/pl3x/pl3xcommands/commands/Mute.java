package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Mute implements MC_Command {
	private MyPlugin plugin;

	public Mute(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.MUTE.get();
	}

	@Override
	public String getCommandName() {
		return "mute";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/mute <player> - " + Lang.MUTE_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7mute &e<&7player&e> &a- &d" + Lang.MUTE_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getOfflinePlayer(args[0]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		if (target.hasPermission("command.mute.exempt")) {
			Pl3xLibs.sendMessage(player, Lang.MUTE_TARGET_EXEMPT.get());
			return;
		}
		PlayerConfig config = PlayerConfig.getConfig(plugin, target.getUUID());
		boolean muted = !config.getBoolean("muted");
		config.set("muted", muted ? "true" : "false");
		if (muted) {
			plugin.getChat().mutePlayer(target.getUUID());
		} else {
			plugin.getChat().unmutePlayer(target.getUUID());
		}
		MC_Player online = Pl3xLibs.getPlayer(target.getCustomName());
		if (online != null) {
			Pl3xLibs.sendMessage(online, Lang.MUTE_TARGET_MSG.get().replace("{action}", muted ? "muted" : "unmuted"));
		}
		Pl3xLibs.sendMessage(player, Lang.MUTE_PLAYER_MSG.get().replace("{action}", muted ? "muted" : "unmuted").replace("{target}", target.getCustomName()));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.mute");
	}
}
