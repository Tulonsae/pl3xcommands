package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class DelHome implements MC_Command {
	public DelHome(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.DELHOME.get();
	}

	@Override
	public String getCommandName() {
		return "delhome";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/delhome ((player:)home) - " + Lang.DELHOME_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.delhome.other")) {
			return Pl3xLibs.colorize("&e/&7delhome &e((&7player&b:&e)&7home&e) &a- &d" + Lang.DELHOME_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7delhome &e(&7home&e) &a- &d" + Lang.DELHOME_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[0]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		String homeName = (args.length > 0 ? args[0].trim() : "home").toLowerCase();
		boolean deleted = false;
		if (homeName.contains(":")) {
			if (!player.hasPermission("command.delhome.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			String[] ss = homeName.split(":");
			if (ss.length < 2) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
				Pl3xLibs.sendMessage(player, getHelpLine(player));
				return;
			}
			MC_Player target = Pl3xLibs.getOfflinePlayer(ss[0]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.delhome.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.DELHOME_TARGET_EXEMPT.get());
				return;
			}
			homeName = ss[1];
			deleted = Home.deleteHomeLocation(target.getUUID(), homeName);
		} else {
			deleted = Home.deleteHomeLocation(player.getUUID(), homeName);
		}
		if (!deleted) {
			Pl3xLibs.sendMessage(player, Lang.DELHOME_HOME_NOT_FOUND.get());
			return;
		}
		Pl3xLibs.sendMessage(player, Lang.DELHOME_HOME_DELETED.get().replace("{home}", homeName));
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.delhome");
	}
}
