package net.pl3x.pl3xcommands.commands;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Econ implements MC_Command {
	private MyPlugin plugin;

	public Econ(MyPlugin plugin) {
		this.plugin = plugin;
	}

	@Override
	public List<String> getAliases() {
		return Alias.ECON.get();
	}

	@Override
	public String getCommandName() {
		return "econ";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/econ <give | set> <player> <amount> - " + Lang.ECON_HELP_DESC.get());
		}
		return Pl3xLibs.colorize("&e/&7econ &e<&7give &e| &7set&e> <&7player&e> <&7amount&e> &a- &d" + Lang.ECON_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 1) {
			List<String> results = new ArrayList<String>();
			if ("set".startsWith(args[0])) {
				results.add("set");
			}
			if ("give".startsWith(args[0])) {
				results.add("give");
			}
			if ("add".startsWith(args[0])) {
				results.add("add");
			}
		}
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length < 3) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_INVALID_COMMAND_STRUCTURE.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		String action = args[0].trim().toLowerCase();
		if (!action.equals("set") && !action.equals("give") && !action.equals("add")) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_UNKNOWN_MISSING_SUBCOMMAND.get());
			Pl3xLibs.sendMessage(player, getHelpLine(player));
			return;
		}
		MC_Player target = Pl3xLibs.getOfflinePlayer(args[1]);
		if (target == null) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
			return;
		}
		Double amount = null;
		try {
			amount = Double.valueOf(args[2]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.ECON_INVALID_AMOUNT.get());
			return;
		}
		String econSymbol = plugin.getConfig().get("econ-symbol");
		if (action.equals("set")) {
			plugin.getEconomy().setBalance(target.getUUID(), amount);
			Pl3xLibs.sendMessage(player, Lang.ECON_SET_AMOUNT.get().replace("{target}", target.getCustomName()).replace("{amount}", amount.toString()).replace("{econ-symbol}", econSymbol));
		}
		if (action.equals("give") || action.equals("add")) {
			plugin.getEconomy().addAmount(target.getUUID(), amount);
			Pl3xLibs.sendMessage(player, Lang.ECON_GIVE_AMOUNT.get().replace("{target}", target.getCustomName()).replace("{amount}", amount.toString()).replace("{econ-symbol}", econSymbol));
		}
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.econ");
	}
}
