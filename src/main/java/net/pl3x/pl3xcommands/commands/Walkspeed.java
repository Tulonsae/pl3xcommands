package net.pl3x.pl3xcommands.commands;

import java.util.List;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xlibs.Pl3xLibs;
import Pl3xCommands.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class Walkspeed implements MC_Command {
	public Walkspeed(MyPlugin plugin) {
	}

	@Override
	public List<String> getAliases() {
		return Alias.WALKSPEED.get();
	}

	@Override
	public String getCommandName() {
		return "walkspeed";
	}

	@Override
	public String getHelpLine(MC_Player player) {
		if (!hasPermissionToUse(player)) {
			return null;
		}
		if (player == null) {
			return Pl3xLibs.decolorize("/walkspeed <speed> (player) - " + Lang.WALKSPEED_HELP_DESC_OTHER.get());
		}
		if (player.hasPermission("command.walkspeed.other")) {
			return Pl3xLibs.colorize("&e/&7walkspeed &e<&7speed&e> (&7player&e) &a- &d" + Lang.WALKSPEED_HELP_DESC_OTHER.get());
		}
		return Pl3xLibs.colorize("&e/&7walkspeed &e<&7speed&e> &a- &d" + Lang.WALKSPEED_HELP_DESC.get());
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args) {
		if (args.length == 2) {
			return Pl3xLibs.getMatchingOnlinePlayerNames(args[1]);
		}
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args) {
		if (args.length == 0) {
			Pl3xLibs.sendMessage(player, Lang.WALKSPEED_NO_NUMBER.get());
			return;
		}
		float speed;
		try {
			speed = Float.valueOf(args[0]);
		} catch (NumberFormatException e) {
			Pl3xLibs.sendMessage(player, Lang.WALKSPEED_INVALID_NUMBER.get());
			return;
		}
		if (speed < 0 || speed > 10) {
			Pl3xLibs.sendMessage(player, Lang.WALKSPEED_BAD_RANGE_NUMBER.get());
			return;
		}
		if (args.length > 1) {
			if (player != null && !player.hasPermission("command.walkspeed.other")) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_NO_PERM.get());
				return;
			}
			MC_Player target = Pl3xLibs.getPlayer(args[1]);
			if (target == null) {
				Pl3xLibs.sendMessage(player, Lang.ERROR_PLAYER_NOT_FOUND.get());
				return;
			}
			if (target.hasPermission("command.walkspeed.exempt")) {
				Pl3xLibs.sendMessage(player, Lang.WALKSPEED_TARGET_EXEMPT.get());
				return;
			}
			target.setWalkSpeed(speed / 10F);
			Pl3xLibs.sendMessage(target, Lang.WALKSPEED_SPEED_SET.get());
			Pl3xLibs.sendMessage(player, Lang.WALKSPEED_SPEED_SET_OTHER.get().replace("{target}", target.getCustomName()));
			return;
		}
		player.setWalkSpeed(speed / 10F);
		Pl3xLibs.sendMessage(player, Lang.WALKSPEED_SPEED_SET.get());
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player) {
		if (player == null) {
			return true; // ALLOW CONSOLE
		}
		return player.hasPermission("command.walkspeed");
	}
}
