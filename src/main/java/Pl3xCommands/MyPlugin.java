package Pl3xCommands;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.pl3x.pl3xcommands.Alias;
import net.pl3x.pl3xcommands.Chat;
import net.pl3x.pl3xcommands.Economy;
import net.pl3x.pl3xcommands.Lang;
import net.pl3x.pl3xcommands.MailBox;
import net.pl3x.pl3xcommands.ServerStatus;
import net.pl3x.pl3xcommands.TextFile;
import net.pl3x.pl3xcommands.commands.Back;
import net.pl3x.pl3xcommands.commands.Balance;
import net.pl3x.pl3xcommands.commands.BalanceTop;
import net.pl3x.pl3xcommands.commands.Broadcast;
import net.pl3x.pl3xcommands.commands.ClearInventory;
import net.pl3x.pl3xcommands.commands.Coords;
import net.pl3x.pl3xcommands.commands.Deafen;
import net.pl3x.pl3xcommands.commands.DelHome;
import net.pl3x.pl3xcommands.commands.DelWarp;
import net.pl3x.pl3xcommands.commands.Econ;
import net.pl3x.pl3xcommands.commands.Fly;
import net.pl3x.pl3xcommands.commands.Flyspeed;
import net.pl3x.pl3xcommands.commands.Global;
import net.pl3x.pl3xcommands.commands.God;
import net.pl3x.pl3xcommands.commands.Hat;
import net.pl3x.pl3xcommands.commands.Home;
import net.pl3x.pl3xcommands.commands.Ignore;
import net.pl3x.pl3xcommands.commands.Kill;
import net.pl3x.pl3xcommands.commands.KillAll;
import net.pl3x.pl3xcommands.commands.ListHomes;
import net.pl3x.pl3xcommands.commands.Local;
import net.pl3x.pl3xcommands.commands.MOTD;
import net.pl3x.pl3xcommands.commands.Mail;
import net.pl3x.pl3xcommands.commands.Msg;
import net.pl3x.pl3xcommands.commands.Mute;
import net.pl3x.pl3xcommands.commands.MuteAll;
import net.pl3x.pl3xcommands.commands.Near;
import net.pl3x.pl3xcommands.commands.Nick;
import net.pl3x.pl3xcommands.commands.Pay;
import net.pl3x.pl3xcommands.commands.Payday;
import net.pl3x.pl3xcommands.commands.Ping;
import net.pl3x.pl3xcommands.commands.Pl3xCommands;
import net.pl3x.pl3xcommands.commands.Rename;
import net.pl3x.pl3xcommands.commands.Reply;
import net.pl3x.pl3xcommands.commands.Rules;
import net.pl3x.pl3xcommands.commands.SetHome;
import net.pl3x.pl3xcommands.commands.SetSpawn;
import net.pl3x.pl3xcommands.commands.SetWarp;
import net.pl3x.pl3xcommands.commands.SetWorldSpawn;
import net.pl3x.pl3xcommands.commands.Smite;
import net.pl3x.pl3xcommands.commands.SocialSpy;
import net.pl3x.pl3xcommands.commands.Spawn;
import net.pl3x.pl3xcommands.commands.Sudo;
import net.pl3x.pl3xcommands.commands.Suicide;
import net.pl3x.pl3xcommands.commands.Teleport;
import net.pl3x.pl3xcommands.commands.Top;
import net.pl3x.pl3xcommands.commands.Tp2p;
import net.pl3x.pl3xcommands.commands.Tpa;
import net.pl3x.pl3xcommands.commands.Tpaall;
import net.pl3x.pl3xcommands.commands.Tpaccept;
import net.pl3x.pl3xcommands.commands.Tpahere;
import net.pl3x.pl3xcommands.commands.Tpall;
import net.pl3x.pl3xcommands.commands.Tpdeny;
import net.pl3x.pl3xcommands.commands.Tphere;
import net.pl3x.pl3xcommands.commands.Tppos;
import net.pl3x.pl3xcommands.commands.Tptoggle;
import net.pl3x.pl3xcommands.commands.Universe;
import net.pl3x.pl3xcommands.commands.Walkspeed;
import net.pl3x.pl3xcommands.commands.Warp;
import net.pl3x.pl3xcommands.commands.World;
import net.pl3x.pl3xcommands.configuration.PlayerConfig;
import net.pl3x.pl3xcommands.configuration.WarpConfig;
import net.pl3x.pl3xcommands.runnables.AutoSaveData;
import net.pl3x.pl3xcommands.runnables.ExecuteCommand;
import net.pl3x.pl3xcommands.runnables.MailChecker;
import net.pl3x.pl3xcommands.runnables.PerformPayday;
import net.pl3x.pl3xcommands.runnables.StartMetrics;
import net.pl3x.pl3xcommands.runnables.StatusRotate;
import net.pl3x.pl3xcommands.runnables.TeleportPlayer;
import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import PluginReference.MC_ArmorStandActionType;
import PluginReference.MC_Block;
import PluginReference.MC_Command;
import PluginReference.MC_ContainerType;
import PluginReference.MC_DamageType;
import PluginReference.MC_DirectionNESWUD;
import PluginReference.MC_Entity;
import PluginReference.MC_EventInfo;
import PluginReference.MC_GeneratedColumn;
import PluginReference.MC_HangingEntityType;
import PluginReference.MC_ItemFrameActionType;
import PluginReference.MC_ItemStack;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_PotionEffectType;
import PluginReference.MC_Server;
import PluginReference.MC_Sign;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private Logger logger = null;
	private BaseConfig config = null;
	private BaseConfig dataConfig = null;
	private WarpConfig warpConfig = null;
	private Chat chat = null;
	private Economy economy = null;
	private ServerStatus serverStatus = null;
	private MC_Server server;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xCommands";
		info.version = "0.1-BETA11";
		info.description = "A collection of useful commands for any server";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		registerCommands();
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	public Logger getLogger() {
		if (logger == null) {
			logger = Pl3xLibs.getLogger(getPluginInfo());
		}
		return logger;
	}

	public MC_Server getServer() {
		return server;
	}

	public Chat getChat() {
		return chat;
	}

	public Economy getEconomy() {
		return economy;
	}

	public ServerStatus getServerStatus() {
		return serverStatus;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public WarpConfig getWarpConfig() {
		if (warpConfig == null) {
			warpConfig = new WarpConfig(this);
		}
		return warpConfig;
	}

	public BaseConfig getDataConfig() {
		if (dataConfig == null) {
			dataConfig = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "datastore.ini");
			dataConfig.load(false);
		}
		return dataConfig;
	}

	private void init() {
		getConfig(); // initialize the config.ini file
		getDataConfig(); // initialize the save data file

		TextFile.copyFileFromJarToDisk(this, "lang-en.ini", true); // always overwrite default language file!
		TextFile.copyFileFromJarToDisk(this, "aliases.ini");
		TextFile.copyFileFromJarToDisk(this, "rules.txt");
		TextFile.copyFileFromJarToDisk(this, "motd.txt");
		TextFile.copyFileFromJarToDisk(this, "server-status.txt");

		chat = new Chat(this);
		economy = new Economy(this);
		serverStatus = new ServerStatus(this);

		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new MailChecker(this), 20, getConfig().getInteger("mail-check-delay", 300) * 20);
		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new StatusRotate(this), 0, getConfig().getInteger("status-rotate-delay", 5) * 20);
		Payday.task = Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new PerformPayday(this), 20, getConfig().getInteger("payday-delay", 3600) * 20);
		Pl3xLibs.getScheduler().scheduleRepeatingTask(getPluginInfo().name, new AutoSaveData(this), 20, getConfig().getInteger("auto-save-delay", 15) * 1200);
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	private void disable() {
		Pl3xLibs.getScheduler().cancelAllRunnables(getPluginInfo().name);
		dataConfig.save();
		warpConfig.save();
		PlayerConfig.saveAllConfigs();
		PlayerConfig.removeAllConfigs();
		config = null;
		dataConfig = null;
		warpConfig = null;
		economy = null;
		serverStatus = null;
		logger = null;
		chat = null;
	}

	public void reload() {
		disable();
		init();
		Alias.refreshAll();
		Lang.refreshAll();
	}

	private void registerCommands() {
		registerCommand(new Back(this));
		registerCommand(new Balance(this));
		registerCommand(new BalanceTop(this));
		// registerCommand(new Buy(this));
		registerCommand(new Broadcast(this));
		registerCommand(new ClearInventory(this));
		registerCommand(new Coords(this));
		registerCommand(new Deafen(this));
		registerCommand(new DelHome(this));
		registerCommand(new DelWarp(this));
		registerCommand(new Econ(this));
		registerCommand(new Fly(this));
		registerCommand(new Flyspeed(this));
		registerCommand(new Global(this));
		registerCommand(new God(this));
		registerCommand(new Hat(this));
		registerCommand(new Home(this));
		registerCommand(new Ignore(this));
		registerCommand(new Kill(this));
		registerCommand(new KillAll(this));
		registerCommand(new ListHomes(this));
		registerCommand(new Local(this));
		registerCommand(new Mail(this));
		registerCommand(new MOTD(this));
		registerCommand(new Msg(this));
		registerCommand(new Mute(this));
		registerCommand(new MuteAll(this));
		registerCommand(new Near(this));
		registerCommand(new Nick(this));
		registerCommand(new Pay(this));
		registerCommand(new Payday(this));
		registerCommand(new Ping(this));
		registerCommand(new Pl3xCommands(this));
		// registerCommand(new PowerTool(this));
		registerCommand(new Rename(this));
		registerCommand(new Reply(this));
		registerCommand(new Rules(this));
		// registerCommand(new Sell(this));
		registerCommand(new SetHome(this));
		registerCommand(new SetSpawn(this));
		registerCommand(new SetWarp(this));
		registerCommand(new SetWorldSpawn(this));
		// registerCommand(new SetWorth(this));
		registerCommand(new Smite(this));
		registerCommand(new SocialSpy(this));
		registerCommand(new Spawn(this));
		registerCommand(new Sudo(this));
		registerCommand(new Suicide(this));
		registerCommand(new Top(this));
		registerCommand(new Teleport(this));
		registerCommand(new Tp2p(this));
		registerCommand(new Tpa(this));
		registerCommand(new Tpaall(this));
		registerCommand(new Tpaccept(this));
		registerCommand(new Tpahere(this));
		registerCommand(new Tpall(this));
		registerCommand(new Tpdeny(this));
		registerCommand(new Tphere(this));
		registerCommand(new Tppos(this));
		registerCommand(new Tptoggle(this));
		registerCommand(new Universe(this));
		registerCommand(new Walkspeed(this));
		registerCommand(new Warp(this));
		registerCommand(new World(this));
		// registerCommand(new Worth(this));
	}

	private void registerCommand(MC_Command command) {
		String commandName = command.getCommandName();
		List<String> list = getConfig().getStringList("disabled-commands");
		if (list != null && list.contains(commandName)) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Disabled command NOT registered: " + commandName);
			}
			return;
		}
		getServer().registerCommand(command);
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Registered command: " + commandName);
		}
	}

	@Override
	public void onPlayerLogin(String playerName, UUID uuid, String ip) {
	}

	@Override
	public void onPlayerLogout(String playerName, UUID uuid) {
		// remove player from TPA db
		for (Iterator<Map.Entry<UUID, UUID>> iter = Tpa.tpadb.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<UUID, UUID> entry = iter.next();
			if (entry.getKey().equals(uuid) || entry.getValue().equals(uuid)) {
				iter.remove();
			}
		}
		// remove player from TPAHERE db
		for (Iterator<Map.Entry<UUID, UUID>> iter = Tpahere.tpaheredb.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<UUID, UUID> entry = iter.next();
			if (entry.getKey().equals(uuid) || entry.getValue().equals(uuid)) {
				iter.remove();
			}
		}
		// remove player from BACK db
		if (Back.backdb.containsKey(playerName)) {
			Back.backdb.remove(playerName);
		}
		// remove player from spy mode
		if (SocialSpy.socialSpyMode.contains(uuid)) {
			SocialSpy.socialSpyMode.remove(uuid);
		}
		// Save last known location
		MC_Player player = Pl3xLibs.getPlayer(uuid);
		if (player != null) {
			PlayerConfig.getConfig(this, uuid).set("last-location", Pl3xLibs.locationToString(player.getLocation()));
		}
	}

	@Override
	public void onInteracted(MC_Player plr, MC_Location loc, MC_ItemStack isHandItem) {
	}

	@Override
	public void onItemPlaced(MC_Player plr, MC_Location loc, MC_ItemStack isHandItem, MC_Location locPlacedAgainst, MC_DirectionNESWUD dir) {
	}

	@Override
	public void onBlockBroke(MC_Player plr, MC_Location loc, int blockKey) {
	}

	@Override
	public void onPlayerDeath(MC_Player plrVictim, MC_Player plrKiller, MC_DamageType dmgType, String deathMsg) {
		Back.backdb.put(plrVictim.getCustomName(), plrVictim.getLocation());
		if (getConfig().getBoolean("teleport-to-world-spawn-on-death", false)) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Player died. Will respawn in same world");
			}
			World.respawnLocs.put(plrVictim.getUUID(), Pl3xLibs.getWorldSpawn(plrVictim.getWorld()));
		} else {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Player died. Will respawn at server spawn!");
			}
			World.respawnLocs.put(plrVictim.getUUID(), Pl3xLibs.getServerSpawn());
		}
	}

	@Override
	public void onPlayerRespawn(MC_Player player) {
		// TODO use player's bed as spawn point if applicable
		MC_Location respawn = World.respawnLocs.get(player.getUUID());
		if (respawn == null) {
			respawn = Pl3xLibs.getServerSpawn(); // this should never happen, but just in case
		}
		if (getConfig().getBoolean("debug-mode", false)) {
			getLogger().debug("Player respawned: " + Pl3xLibs.locationToString(player.getLocation()));
		}
		Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, respawn), 1);
	}

	@Override
	public void onPlayerInput(MC_Player player, String input, MC_EventInfo event) {
		if (getConfig().getBoolean("disable-chat-support", false)) {
			return; // chat support is disabled
		}
		if (input.startsWith("/")) {
			return; // filter out commands
		}
		if (!player.hasPermission("command.local")) {
			Pl3xLibs.sendMessage(player, Lang.ERROR_NO_CHAT_PERMS.get());
		} else {
			getChat().sendChat(player, Chat.ChatChannel.DEFAULT, input);
		}
		event.isCancelled = true; // cancel event so other plugins and the server dont process it.
	}

	@Override
	public void onConsoleInput(String cmd, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptBlockBreak(MC_Player plr, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPlaceOrInteract(MC_Player plr, MC_Location loc, MC_EventInfo ei, MC_DirectionNESWUD dir) {
	}

	@Override
	public void onAttemptExplosion(MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptDamageHangingEntity(MC_Player plr, MC_Location loc, MC_HangingEntityType entType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptItemFrameInteract(MC_Player plr, MC_Location loc, MC_ItemFrameActionType actionType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPotionEffect(MC_Player plr, MC_PotionEffectType potionType, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptPlayerTeleport(MC_Player player, MC_Location loc, MC_EventInfo ei) {
		if (ei.isCancelled) {
			return;
		}
		if (loc.dimension == -1 && (int) loc.y >= 128 && !player.hasPermission("netherroof.exempt")) {
			player.teleport(Pl3xLibs.getWorldSpawn(player.getWorld()));
			Pl3xLibs.sendMessage(player, Lang.NETHERROOF_NOT_ALLOWED.get());
			ei.isCancelled = true;
			return;
		}
		MC_Location previous = Back.backdb.get(player.getCustomName());
		if (previous != null && player.getLocation().equals(previous)) {
			Back.backdb.remove(player.getCustomName());
			return;
		}
		Back.backdb.put(player.getCustomName(), player.getLocation());
	}

	@Override
	public void onAttemptPlayerChangeDimension(MC_Player plr, int newDimension, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptItemDrop(MC_Player plr, MC_ItemStack is, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptAttackEntity(MC_Player plr, MC_Entity ent, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptEntityDamage(MC_Entity ent, MC_DamageType dmgType, double amt, MC_EventInfo ei) {
	}

	@Override
	public void onGenerateWorldColumn(int x, int z, MC_GeneratedColumn data) {
	}

	@Override
	public void onAttemptPistonAction(MC_Location loc, MC_DirectionNESWUD dir, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptBlockFlow(MC_Location loc, MC_Block blk, MC_EventInfo ei) {
	}

	@Override
	public void onContainerOpen(MC_Player plr, List<MC_ItemStack> items, String internalClassName) {
	}

	@Override
	public void onAttemptPlayerMove(MC_Player player, MC_Location from, MC_Location to, MC_EventInfo ei) {
		if (to.dimension == -1 && (int) to.y >= 128 && !player.hasPermission("netherroof.exempt")) {
			player.teleport(Pl3xLibs.getWorldSpawn(player.getWorld()));
			Pl3xLibs.sendMessage(player, Lang.NETHERROOF_NOT_ALLOWED.get());
			ei.isCancelled = true;
		}
	}

	@Override
	public void onPacketSoundEffect(MC_Player plr, String soundName, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onPlayerJoin(MC_Player player) {
		UUID uuid = player.getUUID();
		PlayerConfig config = PlayerConfig.getConfig(this, uuid);
		MC_Location lastLocation = config.getLocation("last-location");
		if (lastLocation == null) {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("New user: Sending to spawn!");
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, Pl3xLibs.getServerSpawn()), 1);
			if (getConfig().getBoolean("announce-new-users", true)) {
				for (MC_Player online : getServer().getPlayers()) {
					if (Pl3xLibs.decolorize(online.getCustomName()).equalsIgnoreCase(Pl3xLibs.decolorize(player.getCustomName()))) {
						continue; // do not send welcome message to the new user
					}
					Pl3xLibs.sendMessage(online, Lang.WELCOME_NEW_USER.get().replace("{player}", player.getCustomName()));
				}
			}
			String runCommand = getConfig().get("new-player-execute-command", null);
			if (runCommand != null && runCommand != "") {
				Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new ExecuteCommand(player, runCommand), 20);
			}
			getEconomy().getBalance(uuid); // sets the starting balance
		} else {
			if (getConfig().getBoolean("debug-mode", false)) {
				getLogger().debug("Repeat user: Sending to last known location!");
			}
			Pl3xLibs.getScheduler().scheduleTask(getPluginInfo().name, new TeleportPlayer(this, player, lastLocation), 1);
		}
		if (player.hasPermission("command.motd.onjoin")) {
			List<String> motd = TextFile.getLineByLine(this, "motd.txt");
			if (motd != null) {
				for (String line : MOTD.replaceVars(this, motd, player)) {
					Pl3xLibs.sendMessage(player, line);
				}
			}
		}
		String nickname = config.get("nickname");
		if (nickname != null && !nickname.equals("")) {
			player.setCustomName(nickname);
		}
		if (player.hasPermission("command.rules.onjoin")) {
			List<String> rules = TextFile.getLineByLine(this, "rules.txt");
			if (rules != null) {
				for (String rule : rules) {
					Pl3xLibs.sendMessage(player, rule);
				}
			}
		}
		MailBox mailbox = MailBox.getMailBox(this, uuid);
		if (!mailbox.refreshMail().isEmpty()) {
			Pl3xLibs.sendMessage(player, Lang.MAIL_NOTIFICATION_ALERT.get());
		}
		if (config.getBoolean("muted", false)) {
			getChat().mutePlayer(uuid);
		}
		if (config.getBoolean("deaf", false)) {
			getChat().deafenPlayer(uuid);
		}
		// retrieve player's ignore list
		List<String> ignoreStringList = config.getStringList("ignoring");
		if (ignoreStringList != null && !ignoreStringList.isEmpty()) {
			List<UUID> ignoreList = new ArrayList<UUID>();
			for (String ignored : ignoreStringList) {
				if (ignored == null || ignored.equals("")) {
					continue; // config contains empty ignore list "ignored=" skip it
				}
				ignoreList.add(UUID.fromString(ignored));
			}
			getChat().setIgnoreList(player.getUUID(), ignoreList);
		}
	}

	@Override
	public void onSignChanging(MC_Player plr, MC_Sign sign, MC_Location loc, List<String> neWLines, MC_EventInfo ei) {
	}

	@Override
	public void onSignChanged(MC_Player plr, MC_Sign sign, MC_Location loc) {
	}

	@Override
	public void onAttemptEntitySpawn(MC_Entity ent, MC_EventInfo ei) {
	}

	@Override
	public void onServerFullyLoaded() {
	}

	@Override
	public void onAttemptHopperReceivingItem(MC_Location loc, MC_ItemStack is, boolean isMinecartHopper, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptBookChange(MC_Player plr, List<String> bookContent, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptCropTrample(MC_Entity ent, MC_Location loc, MC_EventInfo ei) {
	}

	@Override
	public void onFallComplete(MC_Entity ent, float fallDistance, MC_Location loc, boolean isWaterLanding) {
	}

	@Override
	public void onAttemptArmorStandInteract(MC_Player plr, MC_Entity entStand, MC_ArmorStandActionType actionType, MC_EventInfo ei) {
	}

	@Override
	public void onNonPlayerEntityDeath(MC_Entity entVictim, MC_Entity entKiller, MC_DamageType dmgType) {
	}

	@Override
	public void onAttemptItemUse(MC_Player plr, MC_ItemStack is, MC_EventInfo ei) {
	}

	@Override
	public void onContainerClosed(MC_Player plr, MC_ContainerType containerType) {
	}

	@Override
	public void onAttemptItemPickup(MC_Player plr, MC_ItemStack is, boolean isXpOrb, MC_EventInfo ei) {
	}

	@Override
	public void onAttemptEntityInteract(MC_Player plr, MC_Entity ent, MC_EventInfo ei) {
	}
}
